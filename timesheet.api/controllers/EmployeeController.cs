﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.model;

namespace timesheet.api.controllers
{
    [EnableCors("CorsPolicy")]
    [Route("api/v1/employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeService _employeeService;
        public EmployeeController(EmployeeService employeeService)
        {
            this._employeeService = employeeService;
        }

        [HttpGet("getall")]
        public IActionResult GetAll(string text)
        {
            var items = this._employeeService.GetEmployees();
            return new ObjectResult(items);
        }

        [HttpGet("getweekofEmployee/{Id}")]
        public IActionResult GetWeekOfEmployee(int  id)
        {
            var items = this._employeeService.GetWeekOfEmployee(id);
            return new ObjectResult(items);
        }

        [HttpPost("SaveWeekOfEmployee")]
        public IActionResult SaveWeekOfEmployee([FromBody]WeekOfEmployee week)
        {
            try
            {
                if (week==null)
                {
                    return BadRequest("week object is null");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                this._employeeService.SaveWeekOfEmployee(week);

                return new ObjectResult(week);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

    }
}