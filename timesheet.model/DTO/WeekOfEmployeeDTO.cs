﻿namespace timesheet.model
{
    public class WeekOfEmployeeDTO
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int TaskId { get; set; }
        public string Name { get; set; }
        public string TaskName { get; set; }
        public string Code { get; set; }
        public float TotalEffort { get; set; }
        public float AverageEffort { get; set; }

        public float Sunday { get; set; }
        public float Monday { get; set; }
        public float Tuesday { get; set; }
        public float Wednesday { get; set; }
        public float Thursday { get; set; }
        public float Friday { get; set; }
        public float Saturday { get; set; }
    }
}