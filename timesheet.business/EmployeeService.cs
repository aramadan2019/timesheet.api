﻿using System;
using System.Collections.Generic;
using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeService
    {
        public TimesheetDb db { get; }
        public EmployeeService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public List<WeekOfEmployeeDTO> GetEmployees()
        {
            var Employees = (from e in db.Employees
                             select new WeekOfEmployeeDTO
                             {
                                 Id = e.Id,
                                 Name = e.Name,
                                 Code = e.Code,
                                 TotalEffort = db.WeekOfEmployees.Where(w=>w.EmployeeId==e.Id).Select(x => new { total = (x.Sunday + x.Monday + x.Tuesday + x.Wednesday + x.Thursday + x.Friday + x.Saturday) }).DefaultIfEmpty().Sum(x=>x.total),
                                 AverageEffort = db.WeekOfEmployees.Where(w => w.EmployeeId == e.Id).Select(x => new { total = (x.Sunday + x.Monday + x.Tuesday + x.Wednesday + x.Thursday + x.Friday + x.Saturday) }).DefaultIfEmpty().Average(x => x.total),
                             }
                           ).ToList();
            return Employees;
        }

        public IQueryable<WeekOfEmployee> GetWeekOfEmployee(int employeeId)
        {
            return this.db.WeekOfEmployees.Where(x => x.EmployeeId == employeeId);
        }

        public void SaveWeekOfEmployee(WeekOfEmployee week)
        {

            var isExistWeek = db.WeekOfEmployees.Any(x => x.Id == week.Id);

            if (isExistWeek)
            {
                
                db.Update(week);
            }
            else
            {

                db.Add(week);
            }

            db.SaveChanges();

        }
    }
}
